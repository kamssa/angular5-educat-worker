import { NgModule } from '@angular/core';
import {Routes, RouterModule, CanDeactivate} from '@angular/router';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {InviteComponent} from './personne/invite/invite/invite.component';
import {AuthGuard} from './shared/garde-acces/matiere/auth.guard';
import {ManageMatiereComponent} from './matiere/manage-matiere/manage-matiere.component';
import {ListeMatiereComponent} from './matiere/liste-matiere/liste-matiere.component';
import {DetailMatiereComponent} from './matiere/detail-matiere/detail-matiere.component';
import {EditerEnseignantComponent} from './personne/enseignant/editer-enseignant/editer-enseignant.component';
import {CreerMatiereComponent} from './matiere/creer-matiere/creer-matiere.component';
import {EditerMatiereComponent} from './matiere/editer-matiere/editer-matiere.component';
import {CanDeactivateGuard} from './shared/garde-acces/matiere/can-desactivate.guard';

const routes: Routes = [
  {path: '', redirectTo: '/accueil', pathMatch: 'full'},
  {path: 'accueil', component: AccueilComponent},
  {path: 'invite', component: InviteComponent},
  {
    path: 'matiere',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ManageMatiereComponent,

      },
      {
        path: 'liste',
        canActivateChild: [AuthGuard],
        component: ListeMatiereComponent,
        children: [
          {
            path: ':id/detail',
            component: DetailMatiereComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: ':id/edite',
            component: EditerMatiereComponent,
          },
          {
            path: 'creer', component: CreerMatiereComponent,
          }

        ]

      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
