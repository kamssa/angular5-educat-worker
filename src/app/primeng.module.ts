import { NgModule } from '@angular/core';

import {
  AutoCompleteModule,
  ButtonModule, CheckboxModule, ConfirmDialogModule, ContextMenuModule, DataTableModule, DialogModule, DropdownModule,
  InputMaskModule,
  MenuModule,
  OrderListModule,
  PanelMenuModule, SharedModule
} from 'primeng/primeng';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [
    ButtonModule,
    PanelMenuModule,
    HttpClientModule,
    MenuModule,
    DataTableModule,
    DialogModule,
    SharedModule,
    DropdownModule,
    CheckboxModule,
    ContextMenuModule,
    ConfirmDialogModule,
    InputMaskModule,
    AutoCompleteModule,
    OrderListModule
  ],
  exports:[
    ButtonModule,
    PanelMenuModule,
    HttpClientModule,
    MenuModule,
    DataTableModule,
    DialogModule,
    SharedModule,
    DropdownModule,
    CheckboxModule,
    ContextMenuModule,
    ConfirmDialogModule,
    InputMaskModule,
    AutoCompleteModule,
    OrderListModule],
  declarations: []
})
export class PrimengModule { }
