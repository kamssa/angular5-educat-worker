import {Component, OnInit} from '@angular/core';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-creer-matiere',
  templateUrl: './creer-matiere.component.html',
  styleUrls: ['./creer-matiere.component.scss']
})
export class CreerMatiereComponent implements OnInit {
  nouveauMatiere: Matiere = new Matiere(null, null, '', '');
  confirm = false;


  constructor(private matiereService: MatiereService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.confirm = true;
    let m: Matiere;
    m = new Matiere(null, null, '', '');
    this.matiereService.ajoutMatiere(this.nouveauMatiere).subscribe(res => {
        m = res.body;
        console.log(m.libelle, 'est enregistrer.');
      }
    );
    this.nouveauMatiere = null;
    this.router.navigate(['/matiere/liste']);
  }

  annuler() {
    this.router.navigate(['matiere/liste']);
  }

  canDeactivate(): boolean {
    if (this.confirm === false && this.nouveauMatiere.libelle !== '') {
      return window.confirm(`Vous voulez vraiment annuler? `);
    }
    return true;
  }
}
