import {Component, OnInit} from '@angular/core';
import {Matiere} from '../../shared/modele/matiere/matiere';

import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {MessageService} from '../../shared/service/message.service';

@Component({
  selector: 'app-liste-matiere',
  templateUrl: './liste-matiere.component.html',
  styleUrls: ['./liste-matiere.component.scss']
})
export class ListeMatiereComponent implements OnInit {
  title = 'LES MATIERES.';
  matieres: Matiere[];
  omatieres: Observable<Matiere[]>;
  messageServiceErreur: string;
  selectedMatiere: Matiere;
  errorMessage: string;
  errorMessageStatus: string;
  successMessage = '';
  searchItemeSource = new BehaviorSubject<string>('');

  constructor(private  matiereService: MatiereService, private route: Router, private  messageService: MessageService) {
  }

  ngOnInit() {
    this.toutesLesMatieres();
    this.omatieres = this.searchItemeSource
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(mc => mc ? this.matiereService.rechercheMatiereParMc(mc)
        : this.matiereService.rechercheMatiereParMc('personne non trouve'));
    // La matiere a été creé.
    this.matiereService.matiereCreer$
      .subscribe(res => {
        this.closeMessage();
        this.matieres.push(res.body);
        this.successMessage = res.messages.toString();

      });
    // envoi
    this.matiereService.matiereModifier$.subscribe(
      res => {
        this.closeMessage();
        this.matieres[this.findSelectedMatiereIndex()] = res.body;
        this.successMessage = res.messages.toString();

      }
    );
    this.matiereService.matiereSupprimer$.subscribe(
      res => {
        this.closeMessage();
        let index: number;
        index = this.findSelectedMatiereIndex();
        this.matieres = this.matieres.filter((val, i) => i !== index);
        this.successMessage = res.messages.toString();
      }
    );

    this.matiereService.matiereFiltre$
      .subscribe(lib => {
          this.search(lib);
        }
      );
    this.messageService.message$.subscribe(msg => {
        this.messageServiceErreur = msg.toString()
        this.closeMessage();
      }
    );

  }

  toutesLesMatieres() {
    this.matiereService.getAllMatieres()
      .subscribe(data => {
          this.matieres = data.body;
        },
        error => {
          if (error.status = 0) {
            this.errorMessageStatus = 'Problème de connexion!';
          } else {
            this.errorMessage = error;
          }
        })
    ;
  }


  search(mc: string) {
    this.searchItemeSource.next(mc);
  }

  onSelect(matiere: Matiere): void {
    this.selectedMatiere = matiere;
    this.route.navigate(['/matiere/liste', matiere.id, 'detail']);
  }

  findSelectedMatiereIndex(): number {
    return this.matieres.indexOf(this.selectedMatiere);
  }

  closeMessage() {
    setTimeout(() => {
      this.successMessage = '';
      this.messageServiceErreur = '';
    }, 5000);
  }


}
