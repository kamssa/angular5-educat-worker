import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageEnseignantComponent } from './manage-enseignant.component';

describe('ManageEnseignantComponent', () => {
  let component: ManageEnseignantComponent;
  let fixture: ComponentFixture<ManageEnseignantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageEnseignantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageEnseignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
