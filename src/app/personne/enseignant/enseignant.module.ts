import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EnseignantComponent} from './enseignant/enseignant.component';
import {DebutEnseignantComponent} from './debut-enseignant/debut-enseignant.component';
import {DetailEnseignantComponent} from './detail-enseignant/detail-enseignant.component';
import {EditerEnseignantComponent} from './editer-enseignant/editer-enseignant.component';
import {ListeEnseignantComponent} from './liste-enseignant/liste-enseignant.component';
import {ManageEnseignantComponent} from './manage-enseignant/manage-enseignant.component';
import {PhotoEnseignantComponent} from './photo-enseignant/photo-enseignant.component';
import {EnseignantRoutingModule} from './enseignant-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatChipsModule, MatExpansionModule, MatIconModule, MatInputModule, MatListModule,
  MatProgressBarModule,
  MatSelectModule, MatSidenavModule,
  MatSnackBarModule,
  MatToolbarModule, MatTooltipModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    EnseignantRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatTooltipModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatSnackBarModule,
    MatListModule
  ],
  declarations: [EnseignantComponent,
    DebutEnseignantComponent,
    DetailEnseignantComponent,
    EditerEnseignantComponent,
    ListeEnseignantComponent,
    ManageEnseignantComponent,
    PhotoEnseignantComponent
  ]
})
export class EnseignantModule {
}
