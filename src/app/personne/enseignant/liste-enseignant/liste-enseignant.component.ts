import {Component, OnInit} from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MessageService} from '../../../shared/service/message.service';

@Component({
  selector: 'app-liste-enseignant',
  templateUrl: './liste-enseignant.component.html',
  styleUrls: ['./liste-enseignant.component.scss']
})
export class ListeEnseignantComponent implements OnInit {
  title = 'la liste des enseignants';
  enseignants: Enseignant[] = [];
  enseignant: Enseignant;
  selectedEnseignant: Enseignant;
  messageSucces: string;
  messageServiceErreur: string;
  statut: number;
  oenseignants: Observable<Enseignant[]>;
  searchEnseignantSource = new BehaviorSubject<string>('');
  pathNullImage = './assets/images/image3.jpg';

  constructor(private ensSevice: EnseignantService, private router: Router, public snackBar: MatSnackBar, private  messageService: MessageService) {
  }

  ngOnInit() {
    this.ensSevice.enseignnantFiltre$
      .subscribe(text => {
        this.search(text);
        console.log('text recuperer', text);
      });
    this.oenseignants = this.searchEnseignantSource
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(mc => mc ? this.ensSevice.rechercheEnseignantParMc(mc)
        : this.ensSevice.rechercheEnseignantParMc(' '));

    this.toutsLesEnseignants();
    this.ensSevice.enseignantCreer$.subscribe(res => {
        this.enseignants.push(res.body);
        this.messageSucces = res.messages.toString();
        this.snackBar.open(this.messageSucces, '', {
          duration: 3000
        });
      }
    );
    this.ensSevice.enseignantModif$.subscribe(res => {
        this.enseignants[this.findSelectedEnseignantIndex()] = res.body;
        this.messageSucces = res.messages.toString();
        this.snackBar.open(this.messageSucces, '', {
          duration: 3000
        });
      }
    );
    this.ensSevice.enseignnantSupprime$.subscribe(
      res => {
        let index: number;
        index = this.findSelectedEnseignantIndex();
        this.enseignants = this.enseignants.filter((val, i) => i !== index);
        this.messageSucces = res.messages.toString();
        this.snackBar.open(this.messageSucces, '', {
          duration: 3000
        });
      });
    this.messageService.message$.subscribe(msg => {
        this.messageServiceErreur = msg.toString();
        this.closeMessage();
    }

    );
  }

  toutsLesEnseignants() {
    this.ensSevice.getAllEnseignants()
      .subscribe(data => {
        this.enseignants = data.body;
        this.statut = data.statut;

      });

  }

  findSelectedEnseignantIndex(): number {
    return this.enseignants.indexOf(this.selectedEnseignant);
  }


  onSelect(enseignant: Enseignant) {
    this.selectedEnseignant = enseignant;
    this.router.navigate(['enseignant/liste', enseignant.id]);
  }

  search(mc: string) {
    this.searchEnseignantSource.next(mc);
  }

  closeMessage() {
    setTimeout(() => {
      this.messageServiceErreur = '';
    }, 5000);
  }
}
