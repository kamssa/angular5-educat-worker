import { Component, OnInit } from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Resultat} from '../../../shared/modele/resultat';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Observable} from 'rxjs/Observable';
import {Adresse} from '../../../shared/modele/adresse/adresse';

@Component({
  selector: 'app-editer-enseignant',
  templateUrl: './editer-enseignant.component.html',
  styleUrls: ['./editer-enseignant.component.scss']
})
export class EditerEnseignantComponent implements OnInit{
  id: number;
  editMode = false;
  enseignant: Enseignant;
  enseignants: Enseignant[];
  enseignantForm: FormGroup;
  resultat: Resultat<Enseignant>;
  succesMessage: string;

  selectedTitre: string;
  titres = [
    {libelle: 'Mlle', name: 'Mlle'},
    {libelle: 'Mme', name: 'Mme'},
    {libelle: 'Mr', name: 'Mr'}
  ];
  typeTelephones = [
    {libelle: 'mobile', name: 'mobile'},
    {libelle: 'bureau', name: 'bureau'},
    {libelle: 'domicile', name: 'domicile'}
  ];

  constructor(private route: ActivatedRoute, private ensService: EnseignantService, private  fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) => {
        this.id = +params.get('id');
        this.editMode = params.get('id') != null;
        if (this.editMode) {

          console.log('dans le ngOnit de edit', this.editMode);
          return this.ensService.getEnseignantById(this.id);

        } else {
          this.newInit();
          return Observable.of(new Resultat(9, ['nouveau enseignant '], new Enseignant()));
        }

      }
    ).subscribe(res => {
      this.resultat = res;
      this.enseignant = res.body;
      if (res.statut === 0) {
        this.initForm();
      }
    });
  }


  onSubmit() {
    if (!this.editMode) {
      console.log('dans le onSubmit de edit', this.editMode);
      let ens: Enseignant;
      ens = this.convertisseur(this.enseignantForm);
      this.ensService.ajoutEnseignant(ens)
        .subscribe(res => {
          this.succesMessage = res.messages.toString();
          console.log(this.succesMessage);
          console.log('enseignant  res ajoute', res.body);
        });

    } else {
      let ensModif: Enseignant;
      ensModif = this.convertisseur(this.enseignantForm);
      this.ensService.modifierEnseignant(ensModif)
        .subscribe(res => {
          console.log('enseignant est modifier', res.body);

        });
    }

    console.log('enseignant form ajouter', this.enseignantForm.value);
    console.log('enseignant ajouter', this.convertisseur(this.enseignantForm));
    this.router.navigate(['enseignant/liste']);

  }


  ajouTelephone() {
    (<FormArray>this.enseignantForm.get('telephones')).push(
      this.fb.group({
        id: [null],
        version: [0],
        type: [''],
        numero: ['']

      })
    );
  }

  removeTephone(i: number) {
    (<FormArray>this.enseignantForm.get('telephones')).removeAt(i);
  }

  annuler() {
    this.router.navigate(['enseignant/liste']);
  }

  private newInit() {
    const ad = new Adresse('', '', '', '', '', '', '');
    const ens = new Enseignant(null, 0, '', '', '', '', null, null, false, null, '', '', '', ad, [], 'EN');
    this.enseignantForm = this.fb.group({
      id: [ens.id],
      version: [ens.version],
      cni: [ens.cni, Validators.required],
      titre: [ens.titre],
      nom: [ens.nom],
      prenom: [ens.prenom],
      statut: [ens.statut],
      specialite: [ens.specialite],
      pathPhoto: [ens.pathPhoto],
      adresse: this.fb.group({
        codePostal: [ens.adresse.codePostal],
        quartier: [ens.adresse.quartier],
        ville: [ens.adresse.ville],
        email: [ens.adresse.email],
        mobile: [ens.adresse.mobile],
        bureau: [ens.adresse.bureau],
        fixe: [ens.adresse.fixe]
      }),
      telephones: this.fb.array([
        this.fb.group({
          id: [null],
          version: [0],
          type: [''],
          numero: ['']

        })])
    });
  }

  private initForm() {
    const telephonesInit = new FormArray([]);
    let ens: Enseignant;
    if (this.editMode) {
      if (this.resultat.statut === 0) {
        ens = this.enseignant;
        if (ens.telephones.length !== 0) {
          for (const tel of ens.telephones) {
            telephonesInit.push(
              this.fb.group({
                type: tel.typeTelephone,
                numero: tel.numero,
                version: tel.version,
                id: tel.id
              })
            );
          }
        }
      }
    }

    this.enseignantForm = this.fb.group({
      id: [this.enseignant.id],
      version: [this.enseignant.version],
      cni: [this.enseignant.cni],
      titre: [this.enseignant.titre],
      nom: [this.enseignant.nom],
      prenom: [this.enseignant.prenom],
      statut: [this.enseignant.statut],
      specialite: [this.enseignant.specialite],
      pathPhoto: [this.enseignant.pathPhoto],
      telephones: telephonesInit,
      adresse: this.fb.group({
        codePostal: [this.enseignant.adresse.codePostal],
        quartier: [this.enseignant.adresse.quartier],
        ville: [this.enseignant.adresse.ville],
        email: [this.enseignant.adresse.email],
        mobile: [this.enseignant.adresse.mobile],
        bureau: [this.enseignant.adresse.bureau],
        fixe: [this.enseignant.adresse.fixe]
      }),

    });
  }

  private convertisseur(fg: FormGroup): Enseignant {
    const ens = new Enseignant(
      fg.value['id'],
      fg.value['version'],
      fg.value['cni'],
      fg.value['titre'],
      fg.value['nom'],
      fg.value['prenom'],
      null,
      null,
      true,
      null,
      fg.value['statut'],
      fg.value['specialite'],
      fg.value['pathPhoto'],
      fg.value['adresse'],
      fg.value['telephones'],
      'EN'
    );
    return ens;
  }
}
