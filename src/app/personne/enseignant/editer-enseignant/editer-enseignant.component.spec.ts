import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerEnseignantComponent } from './editer-enseignant.component';

describe('EditerEnseignantComponent', () => {
  let component: EditerEnseignantComponent;
  let fixture: ComponentFixture<EditerEnseignantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerEnseignantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerEnseignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
