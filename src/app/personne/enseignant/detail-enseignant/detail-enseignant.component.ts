import { Component, OnInit } from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-detail-enseignant',
  templateUrl: './detail-enseignant.component.html',
  styleUrls: ['./detail-enseignant.component.scss']
})
export class DetailEnseignantComponent implements OnInit {
  enseignant: Enseignant;
  pathNullImage = './assets/images/image3.jpg';

  constructor(private ensService: EnseignantService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) =>
      this.ensService.getEnseignantById(+params.get('id')))
      .subscribe(res => {
        this.enseignant = res.body;
      });

  }

  editerEnseignant() {
    this.router.navigate(['/enseignant/liste', this.enseignant.id, 'editer']);
  }

  ajouterPhoto() {
    this.router.navigate(['/enseignant/liste', this.enseignant.id, 'photo']);
  }

  supprimerEnseignant() {
    this.ensService.supprimerEnseignant(this.enseignant.id)
      .subscribe(res => {
        console.log(res.messages);
      });
    this.router.navigate(['/enseignant/liste']);
  }
}
