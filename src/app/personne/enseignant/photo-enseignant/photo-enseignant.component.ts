import {Component, OnInit, ViewChild} from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-photo-enseignant',
  templateUrl: './photo-enseignant.component.html',
  styleUrls: ['./photo-enseignant.component.scss']
})
export class PhotoEnseignantComponent implements OnInit {
  enseignant: Enseignant;
  ensPhotoForm: FormGroup;
  enseignantImageFile: File;

  @ViewChild('enseignantImage') enseignant_image;


  constructor(private enseService: EnseignantService, private fb: FormBuilder, private route: ActivatedRoute, private  router: Router) {

  }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) =>
      this.enseService.getEnseignantById(+params.get('id')))
      .subscribe(res => {
        this.enseignant = res.body;

      });
    this.initForm();
  }

  onSubmit() {
    const image = this.enseignant_image.nativeElement;
    if (image.files && image.files[0]) {
      this.enseignantImageFile = image.files[0];
    }
    const imageFile: File = this.enseignantImageFile;
    this.enseService.enregistrerPhoto(imageFile, this.enseignant.cni)
      .subscribe(data => {
        console.log(data);
      });
    this.router.navigate(['enseignant/liste']);
  }

  initForm() {
    this.ensPhotoForm = this.fb.group({
      ensImg: ['']
    });
  }
  annuler() {
    this.router.navigate(['enseignant/liste']);
  }

}
