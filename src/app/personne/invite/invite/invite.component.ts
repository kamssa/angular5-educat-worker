import {Component, OnInit} from '@angular/core';
import {Iinvite} from '../../../shared/modele/personne/interface/personne/iinvite';
import {Invite} from '../../../shared/modele/personne/invite';

import {Adresse} from '../../../shared/modele/adresse/adresse';
import {InviteService} from '../../../shared/service/personne/invite/invite.service';
import {ConfirmationService, MenuItem, SelectItem} from 'primeng/primeng';


@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  invites: Iinvite[] = [];
  _statut: number;
  invite: Invite;
  selectedInvite: Invite;
  ////dialogue
  displayDialog: boolean;

  ////diologue visible////
  dialogVisible: boolean;

  newInvite: boolean;
  successMesage: string;
  errorMessage: string;
  errorMessageStatus: string;
  statusSucces: number;
  titres: SelectItem[];
  profils: SelectItem[];
  statuts: SelectItem[];
  ///////////////////////
  ///itemes Menu//////
  items: MenuItem[];

  constructor(private  inviteService: InviteService, private  confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.tousInvites();
    ///////////////////////
    ///////////////////////
    this.titres = [];
    this.titres.push({label: 'Selection titre', value: null});
    this.titres.push({label: 'Mme', value: 'Mme'});
    this.titres.push({label: 'Mlle', value: 'Mlle'});
    this.titres.push({label: 'Mr', value: 'Mr'});
    ////////////////////////////////////////////
    this.profils = [];
    this.profils.push({label: 'Selection profil', value: null});
    this.profils.push({label: 'Visiteur', value: 'Visiteur'});
    this.profils.push({label: 'Client', value: 'Client'});
    //////////////////////////////////////////////////////
    this.statuts = [];
    this.statuts.push({label: 'Selection statut', value: null});
    this.statuts.push({label: 'Permanent', value: 'Permanent'});
    this.statuts.push({label: 'Non Permanent', value: 'Non Permanent'});
    ///////////////////////////////////////////////////////////
    this.items = [
      {label: 'supprimer', icon: 'fa-close', command: (event) => this.deleteInvite()}
    ];

  }

  tousInvites() {
    this.inviteService.getAllInvite()
      .subscribe(data => {
          this.invites = data.body;
          this._statut = data.statut;
          console.log(data);

      });
  }
  showDialogToAdd() {
    this.newInvite = true;
    const ad = new Adresse('', '', '', '', '', '', '');
    this.invite = new Invite(null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, ad, null);

    this.displayDialog = true;
  }


  save() {
    let invites = [...this.invites];
    if (this.newInvite) {
      console.log('debut ajout');
      console.log(this.invite);
      this.inviteService.ajoutInvite(this.invite)
        .subscribe(res => {
            if (res.statut === 0) {
              invites.push(res.body);
              this.invites = invites;
              this.successMesage = res.messages.toString();
              this.statusSucces = res.statut;

            } else {
              this.successMesage = res.messages.toString();
              this.statusSucces = res.statut;
            }

          },
          error => this.errorMessage = error);

    }
    else {
      console.log('modif');
      console.log(this.invite);
      this.inviteService.modifierUnInvite(this.invite)
        .subscribe(res => {
            if (res.statut === 0) {
              invites[this.findSelectedInviteIndex()] = res.body;
              this.invites = invites;
              this.successMesage = res.messages.toString();
              this.statusSucces = res.statut;
              console.log(res.body);
            } else {
              this.successMesage = res.messages.toString();
              this.statusSucces = res.statut;
            }

          },
          resError => {
            this.errorMessage = resError;
            console.error(resError);
          }
        );
    }

    this.invite = null;
    this.displayDialog = false;
    this.dialogVisible = false;
    this.closeMessage();

  }

  delete() {
    let index = this.findSelectedInviteIndex();
    this.inviteService.supprimerUnInvite(this.invite.id)
      .subscribe(res => {
        if (res.statut === 0) {
          this.invites = this.invites.filter((val, i) => i != index);
          this.successMesage = res.messages.toString();
          this.statusSucces = res.statut;
          console.log(res);
        } else {
          this.successMesage = res.messages.toString();
          this.statusSucces = res.statut;
        }

      }, erreur => this.errorMessage = erreur);
    this.displayDialog = false;
    this.closeMessage();
  }

  onRowSelect(event) {
    this.newInvite = false;
    this.invite = this.cloneInvite(event.data);
    this.displayDialog = true;
    console.log(event.data);
    console.log(this.invite);
  }

  findSelectedInviteIndex(): number {
    return this.invites.indexOf(this.selectedInvite);
  }

  cloneInvite(i: Iinvite): Invite {
    let adr: Adresse;
    adr = new Adresse(i.adresse.codePostal, i.adresse.quartier, i.adresse.ville, i.adresse.email, i.adresse.mobile, i.adresse.bureau, i.adresse.fixe);
    let invite: Invite;
    invite = new Invite();
    for (const prop in i) {
      invite[prop] = i[prop];
    }
    invite.adresse = adr;
    return invite;
  }

  private closeMessage() {
    setTimeout(() => {
      this.successMesage = '';
      this.statusSucces = null;
      this.errorMessage = '';

    }, 5000);
  }

  deleteInvite() {
    this.confirmDelete();
  }

  confirmDelete() {
    this.confirmationService.confirm({
      message: 'Voulez vous supprimer  ' + this.selectedInvite.nom + this.selectedInvite.prenom,
      header: 'Confirmation de suppression',
      icon: 'fa fa-trash',
      accept: () => {
        this.invite = this.selectedInvite;
        console.log('confirmer');
        console.log(this.invite);
        this.delete();
      }
    })
    ;

  }

  annuler() {
    this.displayDialog = false;
  }

  annulerModif() {
    this.dialogVisible = false;
  }

  showModification(inv: Invite) {
    //this.newInvite = false;
    this.selectedInvite = inv;
    this.invite = this.cloneInvite(inv);
    this.dialogVisible = true;
  }


}
