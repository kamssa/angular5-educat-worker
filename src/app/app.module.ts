import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {ServiceWorkerModule} from '@angular/service-worker';
import {AppComponent} from './app.component';

import {environment} from '../environments/environment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  AutoCompleteModule,
  ButtonModule,
  CheckboxModule,
  ConfirmationService,
  ConfirmDialogModule,
  ContextMenuModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputMaskModule, MenuModule, OrderListModule,
  PanelMenuModule,
  SharedModule
} from 'primeng/primeng';
import {MaterialModule} from './/material.module';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {FootbarComponent} from './shared/cadre/footbar/footbar.component';
import {MenubarComponent} from './shared/cadre/menubar/menubar.component';
import {NavbarComponent} from './shared/cadre/navbar/navbar.component';
import {InviteComponent} from './personne/invite/invite/invite.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InviteService} from './shared/service/personne/invite/invite.service';
import {MessageService} from './shared/service/message.service';
import {CreerMatiereComponent} from './matiere/creer-matiere/creer-matiere.component';
import {DetailMatiereComponent} from './matiere/detail-matiere/detail-matiere.component';
import {EditerMatiereComponent} from './matiere/editer-matiere/editer-matiere.component';
import {ListeMatiereComponent} from './matiere/liste-matiere/liste-matiere.component';
import {ManageMatiereComponent} from './matiere/manage-matiere/manage-matiere.component';
import {EnseignantModule} from './personne/enseignant/enseignant.module';
import {EnseignantService} from './shared/service/personne/enseignant/enseignant.service';
import {MatiereService} from './shared/service/matiere/matiere.service';

import {AuthGuard} from './shared/garde-acces/matiere/auth.guard';
import {CanDeactivateGuard} from './shared/garde-acces/matiere/can-desactivate.guard';
import {FormsModule} from '@angular/forms';
import { PrimengModule } from './/primeng.module';
import {TimingIntercepteur} from './shared/intercepteur/timing-intercepteur';
import {CachingIntercepteur} from './shared/intercepteur/caching-intercepteur';
import {HttpCache} from './shared/intercepteur/cache/http-cache';
import {HttpCaheService} from './shared/intercepteur/cache/http-cahe.service';



@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FootbarComponent,
    MenubarComponent,
    NavbarComponent,
    InviteComponent,
    CreerMatiereComponent,
    DetailMatiereComponent,
    EditerMatiereComponent,
    ListeMatiereComponent,
    ManageMatiereComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
    MaterialModule,
    EnseignantModule,
    PrimengModule
  ],
  providers: [InviteService,
    ConfirmationService,
    MessageService,
    EnseignantService,
    MatiereService,
    AuthGuard,
    CanDeactivateGuard,
    HttpCaheService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TimingIntercepteur,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CachingIntercepteur,
      multi: true,
    }

    ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
