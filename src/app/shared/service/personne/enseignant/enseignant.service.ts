import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Resultat} from '../../../modele/resultat';
import {Enseignant} from '../../../modele/personne/enseignant';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

import {of} from 'rxjs/observable/of';
import {MessageService} from '../../message.service';


@Injectable()
export class EnseignantService {
  private urlEseignant = 'http://localhost:8080/typePersonnes/EN';
  private urlPersonne = 'http://localhost:8080/personnes';
  private urlPhoto = 'http://localhost:8080/photo';
  private urlPhoto1 = 'http://localhost:8080/getPhoto';
  private urlRechercheEns = 'http://localhost:8080/rechePersonneParMc/EN?mc=';
  // observables sources
  private enseignantCreerSource = new Subject<Resultat<Enseignant>>();
  private enseignantModifSource = new Subject<Resultat<Enseignant>>();
  private enseignantFiltreSource = new Subject<string>();
  private enseignantSupprimeSource = new Subject<Resultat<boolean>>();


// observables streams
  enseignantCreer$ = this.enseignantCreerSource.asObservable();
  enseignantModif$ = this.enseignantModifSource.asObservable();
  enseignnantFiltre$ = this.enseignantFiltreSource.asObservable();
  enseignnantSupprime$ = this.enseignantSupprimeSource.asObservable();

  constructor(private  http: HttpClient, private  messageService: MessageService) {
  }

  getAllEnseignants(): Observable<Resultat<Enseignant[]>> {
    return this.http.get<Resultat<Enseignant[]>>(this.urlEseignant)
      .pipe(
        tap(res => {
          this.log(`enseignants recuperes`);
        }),
        catchError(this.handleError<Resultat<Enseignant[]>>('getAllEnseignants', new Resultat<Enseignant[]>(null, [], [])))
      );
  }

  ajoutEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    console.log('methode du service qui ajoute un enseignant', ens);
    return this.http.post<Resultat<Enseignant>>(this.urlPersonne, ens)
      .pipe(
        tap(res => {
          this.log(`enseignant ajoute nom et prenom=${res.body._nomComplet}`);
          this.enseignantCreer(res);
          this.filtreEnseignant(res.body.nomComplet);
        }),
        catchError(this.handleError<Resultat<Enseignant>>('ajoutEnseignant'))
      );


  }

  modifierEnseignant(ensModif: Enseignant): Observable<Resultat<Enseignant>> {
    return this.http.put<Resultat<Enseignant>>(this.urlPersonne, ensModif)
      .pipe(
        tap(res => {
          this.log(`enseignant modifier nom et prenom =${res.body._nomComplet}`)
          this.enseignantModif(res);
          this.filtreEnseignant(res.body.nomComplet);
        }),
        catchError(this.handleError<Resultat<Enseignant>>('modifierEnseignant'))
      );
  }

  // supprimer un enseignant
  supprimerEnseignant(id: number): Observable<Resultat<boolean>> {
    return this.http.delete<Resultat<boolean>>(`${this.urlPersonne}/${id}`)
      .pipe(
        tap(res => {
          this.log(`enseignant supprime id =${id}`);
          this.enseignantsupprime(res);
        }),
        catchError(this.handleError<Resultat<boolean>>('supprimerEnseignant'))
      );
  }

  getEnseignantById(id: number): Observable<Resultat<Enseignant>> {
    return this.http.get<Resultat<Enseignant>>(`${this.urlPersonne}/${id}`)
      .pipe(
        tap(res => {
          this.log(`enseignat trouve  id=${id}`)
        }),
        catchError(this.handleError<Resultat<Enseignant>>('getEnseignantById'))
      );
  }

  enregistrerPhoto(imageFile: File, cni: string): Observable<Resultat<Enseignant>> {

    const formData: FormData = new FormData();
    formData.append('image_photo', imageFile, cni);
    return this.http.post<Resultat<Enseignant>>(this.urlPhoto, formData)
      .pipe(
        tap(res => {
          this.log(`photo ajoute nom et prenom =${res.body._nomComplet}`)
          this.enseignantModif(res);
          this.filtreEnseignant(res.body.nomComplet);
        }),
        catchError(this.handleError<Resultat<Enseignant>>('enregistrerPhoto'))
      );
  }

  rechercheEnseignantParMc(mc: string): Observable<Array<Enseignant>> {
    return this.http.get<Resultat<Array<Enseignant>>> (`${this.urlRechercheEns}${mc}`)
      .pipe(map(res => res.body,
        tap(res =>
          this.log(`enseignant trouve =${res}`))),
        catchError(this.handleError<Array<Enseignant>>('rechercheEnseignantParMc'))
      );
  }

  enseignantCreer(res: Resultat<Enseignant>) {
    console.log('enseignant a ete  creer correctement essaie source');
    this.enseignantCreerSource.next(res);
  }

  enseignantModif(res: Resultat<Enseignant>) {
    this.enseignantModifSource.next(res);
  }

  filtreEnseignant(text: string) {
    this.enseignantFiltreSource.next(text);
  }

  enseignantsupprime(res: Resultat<boolean>) {
    this.enseignantSupprimeSource.next(res);
  }

  private log(message: string) {
    this.messageService.add('enseignantService: ' + message);

  }

  ///////////////////////////////////////////
  // recuper les errurs

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {


      console.error(error);


      this.log(`${operation} non disponible: ${error.message}`);


      return of(result as T);
    };
  }
}
