import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {Resultat} from '../../modele/resultat';
import {Matiere} from '../../modele/matiere/matiere';
import {Observable} from 'rxjs/Observable';
import {tap, map, catchError} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {MessageService} from '../message.service';
import {Enseignant} from '../../modele/personne/enseignant';

@Injectable()
export class MatiereService {
  // observable source
  private urlMatiere = 'http://localhost:8080/matieres';
  private urlRecherche = 'http://localhost:8080/rechemc/?mc=';
  private matiereCreerSource = new Subject<Resultat<Matiere>>();
  private matiereSupprimerSource = new Subject<Resultat<boolean>>();

  private matiereModifierSource = new Subject<Resultat<Matiere>>();

  private matiereFiltreSource = new Subject<string>();

  // comme observateur
  matiereCreer$ = this.matiereCreerSource.asObservable();
  matiereSupprimer$ = this.matiereSupprimerSource.asObservable();
  matiereModifier$ = this.matiereModifierSource.asObservable();
  matiereFiltre$ = this.matiereFiltreSource.asObservable();

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  getAllMatieres(): Observable<Resultat<Matiere[]>> {
    return this.http.get<Resultat<Matiere[]>>(this.urlMatiere)
      .pipe(
        tap(res =>
          this.log(`matieres recuperes`)
        ),
        catchError(this.handleError<Resultat<Matiere[]>>('getAllMatieres', new Resultat<Matiere[]>(null, [], [])))
      );
  }

  ajoutMatiere(matiere: Matiere): Observable<Resultat<Matiere>> {
    return this.http.post<Resultat<Matiere>>(this.urlMatiere, matiere).pipe(
      tap(res => {
        this.log(`matiere ajoute =${res.body.libelle}`);
        this.matiereCreer(res);
        this.matiereFiltre(res.body.libelle);
      }),
      catchError(this.handleError<Resultat<Matiere>>('ajoutMatiere'))
    );
  }

  getMatiereById(id: number): Observable<Resultat<Matiere>> {
    return this.http.get<Resultat<Matiere>>(`${this.urlMatiere}/${id}`)
      .pipe(
        tap(res =>
          this.log(`matiere trouve  id=${id}`)),
        catchError(this.handleError<Resultat<Matiere>>('getMatiereById'))
      );
  }

  rechercheMatiereParMc(mc: string): Observable<Array<Matiere>> {
    return this.http.get<Resultat<Array<Matiere>>>(`${this.urlRecherche}${mc}`)
      .pipe(map(res => res.body,
        tap(res =>
          this.log(`matiere trouve =${res}`))),
        catchError(this.handleError<Array<Matiere>>('rechercheMatiereParMc'))
      );
  }

  modifierUneMatiere(modifMatiere: Matiere): Observable<Resultat<Matiere>> {
    return this.http.put<Resultat<Matiere>>(this.urlMatiere, modifMatiere)
      .pipe(
        tap(res => {
          this.log(`matiere modifier =${res.body.libelle}`);
          this.matiereModifier(res);
          this.matiereFiltre(res.body.libelle);
        }),
        catchError(this.handleError<Resultat<Matiere>>('modifierUneMatiere'))
      );
  }

  supprimerUneMatiere(id: number): Observable<Resultat<boolean>> {
    return this.http.delete<Resultat<boolean>>(`${this.urlMatiere}/${id}`)
      .pipe(
        tap(res => {
          this.log(`matiere supprime id =${id}`);
          this.matiereSupprimer(res);
          this.matiereFiltre('');
        }),
        catchError(this.handleError<Resultat<boolean>>('supprimerUneMatiere'))
      );
  }

  matiereCreer(res: Resultat<Matiere>) {
    this.matiereCreerSource.next(res);
  }

// lorque l'utilisateur est supprime, on ajoute l'info a notre stream
  matiereSupprimer(r: Resultat<boolean>) {
    this.matiereSupprimerSource.next(r);
  }

  matiereModifier(res: Resultat<Matiere>) {
    this.matiereModifierSource.next(res);
  }

  matiereFiltre(libelle: string) {
    this.matiereFiltreSource.next(libelle);
  }

  private log(message: string) {
    this.messageService.add('matiereService: ' + message);

  }

  // recuper les erreurs


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {


      console.error(error);


      this.log(`${operation} non disponible: ${error.message}`);


      return of(result as T);
    };
  }
}
