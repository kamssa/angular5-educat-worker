import { TestBed, inject } from '@angular/core/testing';

import { HttpCaheService } from './http-cahe.service';

describe('HttpCaheService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpCaheService]
    });
  });

  it('should be created', inject([HttpCaheService], (service: HttpCaheService) => {
    expect(service).toBeTruthy();
  }));
});
