import { TestBed, inject } from '@angular/core/testing';

import { TimingIntercepteurService } from './timing-intercepteur';

describe('TimingIntercepteurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimingIntercepteurService]
    });
  });

  it('should be created', inject([TimingIntercepteurService], (service: TimingIntercepteurService) => {
    expect(service).toBeTruthy();
  }));
});
