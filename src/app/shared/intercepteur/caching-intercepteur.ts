import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/empty';
import {HttpCaheService} from './cache/http-cahe.service';

@Injectable()
export class CachingIntercepteur implements HttpInterceptor {

  constructor(private cache: HttpCaheService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // ignorer les requete non get
    if (req.method !== 'GET') {
      return next.handle(req);
    }
// Ce sera un observable de la valeur mise en cache s'il y en a une
    // ou un observable vide sinon. Ça commence vide.
    let maybeCachedResponse: Observable<HttpEvent<any>> = Observable.empty();

    // verifie le cache
    const cachedResponse = this.cache.get(req);

    if (cachedResponse) {
      maybeCachedResponse = Observable.of(cachedResponse);
      console.log('la reponse du cache', maybeCachedResponse);
    }
    // creer un observable mais ne t'abonne pas
    //demande du reseau et mise en cache de la valeur

    const networkResponse = next.handle(req).do(event => {
      // Tout comme avant, vérifiez l'événement HttpResponse et mettez-le en cache.
      if (event instanceof HttpResponse) {
        this.cache.put(req, event);
        console.log('mis a jour du cache', event);
      }
      console.log('reponse du backend', networkResponse);
    });

    //Maintenant, combinez les deux et envoyez d'abord la réponse en cache s'il y a  un,
    // et la réponse du réseau en second lieu
    return Observable.concat(maybeCachedResponse, networkResponse);
  }

}
