import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpHandler, HttpHeaderResponse,
  HttpInterceptor, HttpProgressEvent,
  HttpRequest, HttpResponse,
  HttpSentEvent, HttpUserEvent
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class TimingIntercepteur implements HttpInterceptor {

  constructor() {
  }


  intercept(req: HttpRequest<any>, next: HttpHandler):
  Observable<HttpEvent<any>>
  {
    const started = Date.now();
    return next.handle(req)
      .do(event => {
        if (event instanceof HttpResponse) {
          const elapsed = Date.now() - started;
          console.log(`Request for ${req.urlWithParams} took ${elapsed} ms.`);
        }
      });
  }
  }



