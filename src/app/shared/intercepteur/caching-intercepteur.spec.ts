import { TestBed, inject } from '@angular/core/testing';

import { CachingIntercepteurService } from './caching-intercepteur';

describe('CachingIntercepteurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CachingIntercepteurService]
    });
  });

  it('should be created', inject([CachingIntercepteurService], (service: CachingIntercepteurService) => {
    expect(service).toBeTruthy();
  }));
});
