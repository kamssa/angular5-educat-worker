import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/primeng';

@Component({
  selector: 'educ-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss']
})
export class MenubarComponent implements OnInit {
   items: MenuItem[];

  constructor() {
  }

  ngOnInit() {
    this.items = [
      {
        label: 'ACCUEIL',
        icon: 'fa fa-home',
        items: [
          {label: 'Accueil', icon: 'fa fa-life-ring', routerLink: ['/accueil']},
          {label: 'Invite', icon: 'fa fa-address-book', routerLink: ['/invite']}
        ]
      },
      {
        label: 'FORMATION',
        icon: 'fa-edit',
        items: [
          {label: 'Etudiant', icon: 'fa fa-life-ring', routerLink: ['/etudiant']},
          {label: 'Matiere', icon: 'fa fa-book', routerLink: ['/matiere']},
          {label: 'Cours', icon: 'fa fa-book', routerLink: ['/matiere']}]
      },
      {
        label: 'ADMINISTRATION',
        icon: 'fa-edit',
        items: [
          {label: 'Administrateur', icon: 'fa fa-life-ring', routerLink: ['/administration']},
          {label: 'Enseignant', icon: 'fa fa-book', routerLink: ['/enseignant']},
         ]
      }
    ];


  }

}
