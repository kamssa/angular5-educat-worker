import {Iadresse} from '../personne/interface/personne/iadresse';
export class Adresse implements Iadresse{



  constructor(private _codePostal?: string, private _quartier?: string, private _ville?: string, private _email?: string, private _mobile?: string, private _bureau?: string, private _fixe?: string) {
    this._codePostal = _codePostal;
    this._quartier = _quartier;
    this._ville = _ville;
    this._email = _email;
    this._mobile = _mobile;
    this._bureau = _bureau;
    this._fixe = _fixe;
  }

  get codePostal(): string {
    return this._codePostal;
  }

  set codePostal(value: string) {
    this._codePostal = value;
  }

  get quartier(): string {
    return this._quartier;
  }

  set quartier(value: string) {
    this._quartier = value;
  }

  get ville(): string {
    return this._ville;
  }

  set ville(value: string) {
    this._ville = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get mobile(): string {
    return this._mobile;
  }

  set mobile(value: string) {
    this._mobile = value;
  }

  get bureau(): string {
    return this._bureau;
  }

  set bureau(value: string) {
    this._bureau = value;
  }

  get fixe(): string {
    return this._fixe;
  }

  set fixe(value: string) {
    this._fixe = value;
  }
}
