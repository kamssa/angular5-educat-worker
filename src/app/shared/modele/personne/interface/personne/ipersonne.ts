import {Iadresse} from './iadresse';
export interface Ipersonne {
  id ?: number;
  version?: number;
  titre ?: string;
  nom ?: string;
  prenom ?: string;
  cni ?: string;
  login ?: string;
  password ?: string;
  actived ?: boolean;
  nomComplet ?: string;
  description ?: string;
  adresse ?: Iadresse;
  type?: string;
}
