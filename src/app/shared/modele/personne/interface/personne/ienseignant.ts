import {Ipersonne} from './ipersonne';

export interface IEnseignant extends Ipersonne {
  statut?: string;
  specialite?: string;
}
